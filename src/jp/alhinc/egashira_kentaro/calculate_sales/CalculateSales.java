package jp.alhinc.egashira_kentaro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void inputFile(
			String filePath,
			String fileName,
			Map<String, String>branchNames,
			Map<String, Long>branchSales ) {

	  BufferedReader br = null;
	   try {
		   File file = new File(filePath, fileName);

		   //ファイル名不正検出
		    if(! file.exists()) {
		    	System.out.println("ファイル名が不正です");
		    	return;
		    }
		   FileReader fr = new FileReader(file);
		   br = new BufferedReader(fr);

		   String line;
		   while((line = br.readLine()) != null) {
			   String[] items = line.split(",");

			   branchNames.put(items[0], items[1]);
			   branchSales.put(items[0], 0L);

			   //"$"がなければ4桁目も許容してしまう
			   if(items.length != 2 || !items[0].matches("^[0-9]{3}$")) {
				   System.out.println("支店定義ファイルのフォーマットが不正です");
				   return;
			   }
		   }
	   } catch(IOException e) {
		   System.out.println("エラーが発生しました。");
	   } finally {
		   if(br != null) {
			  try {
				 br.close();
		      } catch(IOException e) {
				 System.out.println("closeできませんでした。");
				  return;
			   	}
		   }
	   	}
	}
	public static void outputFile(
			String filePath,
			String fileName,
			Map<String, String>branchNames,
			Map<String, Long>branchSales) {

	BufferedWriter bw = null;
	try {
		//ファイル名を指定
		File file = new File(filePath, fileName);
    //エディタを開いている状態
		bw =new BufferedWriter(new FileWriter(file));

		for(String key: branchNames.keySet()) {
			bw.write(key +"," + branchNames.get(key)+"," + branchSales.get(key));
			System.out.println(key +"," + branchNames.get(key)+"," + branchSales.get(key));
            //bwを改行
			bw.newLine();
		}
	} catch(IOException e) {
	   System.out.println("エラーが発生しました。");
	   } finally {
		     if(bw != null) {
			   try {
			   //closeしないと保存されていない状態
			   bw.close();
			   } catch(IOException e) {
				  System.out.println("closeできませんでした。");
				  return;
				 }
		     }
	    }
	}
	public static void main(String[] args) {

	   Map<String, String> branchNames = new HashMap<>();
	   Map<String, Long> branchSales = new HashMap<>();

	   if(args.length != 1) {
		   System.out.println("予期せぬエラーが発生しました");
		   return;
	  }

	   inputFile(args[0], "branch.lst", branchNames, branchSales);
	 	   //filesにlistFilesで羅列
	   File[] files = new File(args[0]).listFiles();
	   //ArrayListクラスを変数salesFilesに代入
		List<File> salesFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
		  String fileNames = files[i].getName();
			//正規表現(文頭が数字８桁で.rcdファイルのみ)filesがファイルかつ指定桁数.rcdであるかを判定
			if(files[i].isFile() && fileNames.matches("^[0-9]{8}.rcd$")) {
				salesFiles.add(files[i]);
			}
		}

    //降順でソートををかける(OSによってはソートがかかってない状態でリストされる可能性)
	Collections.sort(salesFiles);
	//売り上げファイル(rcdファイル)の連番チェック
	for(int i = 0; i  < salesFiles.size() - 1 ; i++) {
		int firstData = Integer.parseInt(salesFiles.get(i).getName().substring(0, 8));
		int secondData = Integer.parseInt(salesFiles.get(i + 1).getName().substring(0, 8));

		if((secondData -firstData) != 1) {
			System.out.println( "ファイル名が連番ではありません");
			return;
		}
	}

		for(int i = 0; i < salesFiles.size(); i++) {

			BufferedReader br = null;
			try {
               //リスト(売り上げファイル)から１行ずつ取得
				FileReader fileData = new FileReader(salesFiles.get(i));
				br = new BufferedReader(fileData);
				List<String> allSalesFiles = new ArrayList<>();

				String line;
					while((line = br.readLine()) != null) {
							allSalesFiles.add(line);
					}
				//allSalesFileが数字であるか(正規表現)を判定→long型に変換前に
					  if(!allSalesFiles.get(1).matches("^[0-9]+$")) {
						  System.out.println("予期せぬエラーが発生しました");
						  return;
					  }

					if(allSalesFiles.size() != 2) {
						System.out.println("売り上げファイルのフォーマットが不正です");
						return;
					}

						 if( !branchNames.containsKey(allSalesFiles.get(0))) {
							System.out.println("支店コードが不正です");
							return;
						 }

				//allSalesfilesの一行目(0)を取得
				String branchCode = allSalesFiles.get(0);

				long fileSale = Long.parseLong(allSalesFiles.get(1));
				long saleAmount = branchSales.get(branchCode) + fileSale;

				//１０桁を超えていないかチェック
				if(fileSale >= 10000000000L) {
					System.out.println("合計金額が１０桁をこえました");
					return;
				} else {
				 branchSales.put(branchCode, saleAmount);
					}
		   } catch(IOException e) {
			  System.out.println("エラーが発生しました。");
		      } finally {
			     if(br != null) {
				   try {
				   br.close();
				   } catch(IOException e) {
				  System.out.println("closeできませんでした。");
				  return;
			     }
		     }
	      }
	    }
		outputFile(args[0],"branchOut", branchNames, branchSales);
	}
}